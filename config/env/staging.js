const staging = {
    PORT: process.env.GATEWAY_STAGING_PORT,
    USER_SERVICE_URL: 'http://user-service'
};

export default staging;
