const test = {
    PORT: process.env.GATEWAY_TEST_PORT,
    USER_SERVICE_URL: 'http://user-service'
};

export default test;
