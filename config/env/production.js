const production = {
    PORT: process.env.GATEWAY_PROD_PORT,
    USER_SERVICE_URL: 'http://user-service'
};

export default production;
