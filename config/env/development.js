const development = {
    PORT: process.env.GATEWAY_DEV_PORT,
    USER_SERVICE_URL: 'http://user-service.default.svc.cluster.local'
};

export default development;
