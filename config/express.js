import fs from 'fs';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import FileStreamRotator from 'file-stream-rotator';
import httpProxy from 'express-http-proxy';

import loggerInit from './logger';
import routes from '../app/routes/index';
import authController from '../app/utils/auth/auth.controller';
import encoder from '../app/utils/auth/encoder';
import config from './';


const userServiceProxy = httpProxy(`${config.USER_SERVICE_URL}/`);

const logDirectory = './log';
const checkLogDir = fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

const expressConfig = (app) => {
    let accessLogStream,
        logger;

    if (app.get('env') === 'development') {
        logger = loggerInit('development');
    } else if (app.get('env') === 'production') {
        logger = loggerInit('production');
    } else if (app.get('env') === 'test') {
        logger = loggerInit('test');
    } else {
        logger = loggerInit();
    }

    global.logger = logger;
    logger.info('Application starting...');
    logger.debug("Overriding 'Express' logger");


    if (checkLogDir) {
        accessLogStream = FileStreamRotator.getStream({
            date_format: 'YYYYMMDD',
            filename: `${logDirectory}/gateway-%DATE%.log`,
            frequency: 'weekly',
            verbose: false
        });
    }


    app.use(morgan('combined', { stream: accessLogStream }));


    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));


    app.use(helmet());
    app.disable('x-powered-by');


    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });


    app.use((req, res, next) => {
        const { headers } = req;
        authController(headers)
            .then((response) => {
                const { payload } = response;
                const gateWayAuth = encoder(payload);
                req.headers.gatewayauth = gateWayAuth;
                next();
            })
            .catch((e) => {
                res.status(401)
                    .json({
                        message: e
                    });
            });
    });


    app.use(
        '/v1/user/',
        (req, res, next) => {
            userServiceProxy(req, res, next);
        }
    );


    app.use('/v1/', routes);


    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });


    if (app.get('env') === 'development' || app.get('env') === 'test') {
        app.use((err, req, res, next) =>
            res.status(err.status || 500)
                .json({
                    message: err.message,
                    error: err
                }));
    }

    app.use((err, req, res, next) =>
        res.status(err.status || 500).json({ message: err.message }));
};

export default expressConfig;
