const encoder = (obj) => Buffer.from(JSON.stringify(obj)).toString('base64');

export default encoder;
