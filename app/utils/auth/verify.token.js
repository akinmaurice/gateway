import jwt from 'jsonwebtoken';
import Q from 'q';
import fs from 'fs';
import config from '../../../config';


const publicKey = fs.readFileSync('./public.key', 'utf8');


const verifyOptions = {
    issuer: config.auth.issuer,
    subject: config.auth.subject,
    audience: config.auth.audience,
    maxAge: config.auth.expiresIn,
    algorithms: [ 'RS256' ]
};

const verifyToken = (token) => {
    try {
        return jwt.verify(token, publicKey, verifyOptions);
    } catch (e) {
        return false;
    }
};

const decodeToken = (token) => jwt.decode(token, { complete: true });


const validateAuth = async(token) => {
    const defer = Q.defer();
    try {
        const verifiedToken = await verifyToken(token);
        if (!verifiedToken) {
            defer.reject('Unauthorized');
        }
        const decodedToken = await decodeToken(token);
        defer.resolve(decodedToken);
    } catch (e) {
        defer.reject(e);
    }
    return defer.promise;
};


export default validateAuth;
