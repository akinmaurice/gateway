import Q from 'q';
import validateAuth from './verify.token';

const authController = async(req) => {
    const defer = Q.defer();
    const { authorization } = req;
    if (!authorization) {
        defer.reject('No Authorization token provided');
    }
    const payload = await validateAuth(authorization);
    defer.resolve(payload);
    return defer.promise;
};


export default authController;
