import assert from 'assert';
import request from 'supertest';

import app from '../../index';

describe('Integration test', () => {
    it('Test routes', done => {
        request(app)
            .get('/v1/')
            .set('Content-Type', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                assert.equal(res.body.message, 'Gateway Service');
                done();
            });
    });
});
